package com.wilson.android.library;


/*
 Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import si.evil.redphone.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

/**
 * Improved by
 * @author gcasar
 *
 */
public class DrawableManager {
    private final Map<String, Drawable> drawableMap;
    
    private static final Task END_TASK = new Task();
    
    Context context;
    
    Worker worker;
    
    Handler handler;
    
    /**
     * Queue used to signal
     */
	BlockingQueue<Task> queue = new LinkedBlockingQueue<DrawableManager.Task>(); 

    public DrawableManager(Context c) {
    	context = c;
        drawableMap = new HashMap<String, Drawable>();
        
        worker = new Worker();
        worker.start();

        handler = new Handler(){
	        @Override
	        public void handleMessage(Message message) {
	        	Task t = (Task)message.obj;
	            t.target.setImageDrawable(t.drawable);
	        }
	    };
    }
    
    public void cleanup(){
    	try {
			queue.put(END_TASK);
		} catch (InterruptedException e) {
			//I dont actually know what would happen - background thread would probably not get stopped
		}
    }

    public Drawable fetchDrawable(String urlString) {
        if (drawableMap.containsKey(urlString)) {
            return drawableMap.get(urlString);
        }

    	Drawable drawable;
		try {
			Bitmap bmp = MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.parse(urlString));
	        drawable = new BitmapDrawable(bmp);
		} catch (FileNotFoundException e) {
			drawable = context.getResources().getDrawable(R.drawable.gray_square);
		} catch (IOException e) {
			drawable = context.getResources().getDrawable(R.drawable.gray_square);
		}

		return drawable;
    }

    public void fetchDrawableOnThread(final String urlString, final ImageView imageView) {
        if (drawableMap.containsKey(urlString)) {
            imageView.setImageDrawable(drawableMap.get(urlString));
            return;
        }

        Task task = new Task();
        task.key = urlString;
        task.target = imageView;
        queue.add(task);
        
    }
    
    static class Task{
    	String key;
    	ImageView target;
    	Drawable drawable;
    }
    
    public class Worker extends Thread{
    	@Override
        public void run() {
    		Task todo = null;
            
 
			while(true){
				try {
					if((todo=queue.take())==END_TASK){
						break;
					}
				} catch (InterruptedException e) {
					//nothing serious
					continue;
				}
				final Task task = todo;

			    Drawable drawable = fetchDrawable(task.key);
			    task.drawable = drawable;
			    drawableMap.put(todo.key, drawable);
			    Message message = handler.obtainMessage(1, task);
			    handler.sendMessage(message);
			}
        }
    }

}