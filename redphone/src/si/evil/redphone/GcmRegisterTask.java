package si.evil.redphone;

import java.io.IOException;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import si.evil.redphone.activities.MainActivity;
import si.evil.redphone.data.ContactInfo;
import android.os.AsyncTask;
import android.util.Log;

public class GcmRegisterTask extends AsyncTask<Void, Void, Void>{
	
	
	
	MainActivity context;
	
	public GcmRegisterTask(MainActivity c){
		this.context = c;
	}

	@Override
	protected Void doInBackground(Void... params) {
        try {
        	GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
            
            String regid = gcm.register(MainActivity.SENDER_ID);
            ContactInfo self = context.getSelf();
            
            MainActivity.updateRemoteGCM(context,regid,self.id);


            // Persist the regID - no need to register again.
            context.storeRegistrationId(regid);
        } catch (IOException ex) {
            Log.e("GCM", "Error :" + ex.getMessage());
            // If there is an error, don't just keep trying to register.
            // Require the user to click a button again, or perform
            // exponential back-off.
            //TODO
        }
        return null;
	}

}
