package si.evil.redphone;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

/**
 * Master server manager. Manages locations of various servers. Singleton.
 * @author gcasar
 *
 */
public class MSManager {
	static MSManager instance;
	
	static String STATIC_ADDRESS = "178.62.231.42";
	static int STATIC_PORT = 9999;
	
	InetAddress activeAddress;
	
	MSManager(){
		init();
	}
	
	
	public static MSManager getInstance(){
		if(instance==null){
			instance = new MSManager();
		}
		return instance;
	}
	
	void init(){
		try {
			activeAddress = Inet4Address.getByName(STATIC_ADDRESS);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
	public InetAddress getActiveServerAddress(){
		return activeAddress;
	}
	
	public int getActiveServerPort(){
		return STATIC_PORT;
	}

}
