package si.evil.redphone;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import si.evil.redphone.util.Utils;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;

public class StreamProxyOld implements Runnable {

    private static final int SERVER_PORT=8888;
    
    public static final int IN_PORT = 6666;

	private static final String TAG = "StreamProxy";

    private Thread thread;
    private boolean isRunning;
    private ServerSocket socket;
    private ServerSocket reciever;
    private int port;
    private Context context = null;

    public StreamProxyOld(Context c) {
    	context = c;
        // Create listening socket
        try {
          socket = new ServerSocket(SERVER_PORT, 0, InetAddress.getByAddress(new byte[] {127,0,0,1}));
          socket.setSoTimeout(5000);
          port = socket.getLocalPort();
        } catch (UnknownHostException e) { // impossible
        } catch (IOException e) {
          Log.e(TAG, "IOException initializing server", e);
        }

    }

    public void start() {
        thread = new Thread(this);
        thread.start();
    }

    public void stop() {
        isRunning = false;
        thread.interrupt();
        try {
            thread.join(5000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
    }

    @Override
      public void run() {
        Looper.prepare();
        isRunning = true;
        while (isRunning) {
          try {
            Socket client = socket.accept();
            if (client == null) {
              continue;
            }
            Log.d(TAG, "client connected");

            StreamToMediaPlayerTask task = new StreamToMediaPlayerTask(client,context);
            if (task.processRequest()) {
                task.execute();
            }

          } catch (SocketTimeoutException e) {
            // Do nothing
          } catch (IOException e) {
            Log.e(TAG, "Error connecting to client", e);
          }
        }
        Log.d(TAG, "Proxy interrupted. Shutting down.");
      }




    private class StreamToMediaPlayerTask extends AsyncTask<String, Void, Integer> {

		String localPath;
        Socket client;
        int cbSkip;
        Context context;

        public StreamToMediaPlayerTask(Socket client, Context context) {
            this.client = client;
            this.context = context;
        }

        public boolean processRequest() {
            // Read HTTP headers
            String headers = "";
            try {
              headers = Utils.readTextStreamAvailable(client.getInputStream());
            } catch (IOException e) {
              Log.e(TAG, "Error reading HTTP request header from stream:", e);
              return false;
            }

            // Get the important bits from the headers
            String[] headerLines = headers.split("\n");
            String urlLine = headerLines[0];
            if (!urlLine.startsWith("GET ")) {
                Log.e(TAG, "Only GET is supported");
                return false;               
            }
            urlLine = urlLine.substring(4);
            int charPos = urlLine.indexOf(' ');
            if (charPos != -1) {
                urlLine = urlLine.substring(1, charPos);
            }
            localPath = urlLine;

            // See if there's a "Range:" header
            for (int i=0 ; i<headerLines.length ; i++) {
                String headerLine = headerLines[i];
                if (headerLine.startsWith("Range: bytes=")) {
                    headerLine = headerLine.substring(13);
                    charPos = headerLine.indexOf('-');
                    if (charPos>0) {
                        headerLine = headerLine.substring(0,charPos);
                    }
                    cbSkip = Integer.parseInt(headerLine);
                }
            }
            return true;
        }

        @Override
        protected Integer doInBackground(String... params) {

            long fileSize = Integer.MAX_VALUE;//GET CONTENT LENGTH HERE; A BIG ASS CONTENT LENGTH

            // Create HTTP header
            String headers = "HTTP/1.0 200 OK\r\n";
            headers += "Content-Type: " + /*MIME TYPE HERE*/"audio/mpeg" + "\r\n";
            headers += "Content-Length: " + fileSize  + "\r\n";
            headers += "Connection: close\r\n";
            headers += "\r\n";
            
            Socket client = null;
            
            try {
				reciever = new ServerSocket(IN_PORT);
				while (client == null) {
		          try {
		            client = socket.accept();
		            if (client == null) {
		              continue;
		            }
		            
		            Log.d(TAG, "in client connected"+client.isConnected());
	
		          } catch (SocketTimeoutException e) {
		            // Do nothing
		          } catch (IOException e) {
		            Log.e(TAG, "Error connecting to client", e);
		          }
				}
            } catch (SocketException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				
				return 0;
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

            // Begin with HTTP header
            OutputStream output = null;
            byte[] buf = new byte[64 * 1024];
            char[] cbuf = new char[buf.length/2];
            try {
                output = new BufferedOutputStream(client.getOutputStream(), 32*1024);                           
                output.write(headers.getBytes());

                client.setReceiveBufferSize(buf.length);
                BufferedReader input = new BufferedReader(new InputStreamReader(client.getInputStream()));

  

                // Loop as long as there's stuff to send
                while (isRunning && !client.isClosed()) {


                    Log.d(TAG, "Blocking until more data appears");

                	int length = input.read(cbuf);
                	
                	
                	for(int i=0;i<length;i++) {
            		   buf[i*2] = (byte) (cbuf[i] >> 8);
            		   buf[i*2+1] = (byte) cbuf[i];
            		}
                	
                	length = length*2;

                    Log.d(TAG, "Read "+length);
                    // See if there's more to send
                    if (length>0) {
                        output.write(buf, 0, length);
                        output.flush();
                        cbSkip += length;
                    }else if(length<0){

                        Log.d(TAG, "No more data");
                        break;
                    }

                }
            }
            catch (SocketException socketException) {
                Log.e(TAG, "SocketException() thrown, proxy client has probably closed. This can exit harmlessly");
            }
            catch (Exception e) {
                Log.e(TAG, "Exception thrown from streaming task:");
                Log.e(TAG, e.getClass().getName() + " : " + e.getLocalizedMessage());
                e.printStackTrace();                
            }

            // Cleanup
            try {
                if (output != null) {
                    output.close();
                }
                client.close();
            }
            catch (IOException e) {
                Log.e(TAG, "IOException while cleaning up streaming task:");                
                Log.e(TAG, e.getClass().getName() + " : " + e.getLocalizedMessage());
                e.printStackTrace();                
            }

            return 1;
        }

    }
}