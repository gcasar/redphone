package si.evil.redphone;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;

import android.media.MediaRecorder;
import android.os.ParcelFileDescriptor;
import android.util.Log;

public class StreamRecorderOld implements Runnable {
	
	public static final int PORT = 9999;

	Socket socket;
	
	boolean isRunning = false;
	
	Thread thread;

    StreamRecorderOld() {
    }

    public void start() {
    	isRunning = true;
        thread = new Thread(this);
        thread.start();
    }

    public void stop() {
        isRunning = false;
        thread.interrupt();
        try {
            thread.join(5000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
    }

	@Override
	public void run() {
		 try {
			 Log.e("RECORDER", "STARTING");
			socket = new Socket();
			socket.connect(new InetSocketAddress("127.0.0.1",StreamProxyOld.IN_PORT));
			
			Log.e("RECORDER", "connected"+socket.isConnected());

			ParcelFileDescriptor pfd = ParcelFileDescriptor.fromSocket(socket);

			MediaRecorder recorder = new MediaRecorder();
			recorder.setOutputFile(pfd.getFileDescriptor());
			
			recorder.prepare();
			recorder.start();
			
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
