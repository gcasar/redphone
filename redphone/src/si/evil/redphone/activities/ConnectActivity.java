package si.evil.redphone.activities;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.Collections;
import java.util.List;

import org.apache.http.conn.util.InetAddressUtils;

import si.evil.redphone.R;
import si.evil.redphone.R.id;
import si.evil.redphone.R.layout;
import si.evil.redphone.R.menu;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class ConnectActivity extends Activity {
	
	public static final String SERVER_HOSTNAME = "bigbuckduck.com";
	
	public static final int SERVER_PORT = 8080;
	
	public static final int CLIENT_PORT = 6666;
	
	DatagramSocket socket;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_connect);
		
		TextView view = (TextView) findViewById(R.id.status);
		view.setText(getIPAddress(true));

		try {
			socket = new DatagramSocket(CLIENT_PORT);

			
		    IOLoop io00 = new IOLoop();
		    io00.start();
		    
		    IOth sender = new IOth();
		    sender.start();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		
		
	}
	
	public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase();
                        boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr); 
                        if (useIPv4) {
                            if (isIPv4) 
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 port suffix
                                return delim<0 ? sAddr : sAddr.substring(0, delim);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.connect, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {

		    IOth sender = new IOth();
		    sender.start();
			
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	class IOth extends Thread
    {
        @Override
        public void run()
        {

            String sendMsg = "UDP hole punching";
            byte[] buf = sendMsg.getBytes();
            DatagramPacket packet;

            System.out.println(SERVER_HOSTNAME); // node server IP
            System.out.println(SERVER_PORT); // 15000
            try{
    			
                packet = new DatagramPacket(buf, buf.length, InetAddress.getByName(SERVER_HOSTNAME), SERVER_PORT);
                socket.send(packet);

                packet = new DatagramPacket(buf, buf.length, InetAddress.getByName("86.61.12.73"), 65220);
                socket.send(packet);
                

                packet = new DatagramPacket(buf, buf.length, InetAddress.getByName("192.168.2.32"), 65220);
                socket.send(packet);



            }catch (Exception e){
                System.out.println(e);
            }
        }
    }
	
	class IOLoop extends Thread
    {
        @Override
        public void run()
        {
            try
            {
                String msg = "Native.UDPserver.open";

    			
                SocketAddress sockAddress;
                String address;


                byte[] buf = new byte[1024];
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                while (true)
                {
                    try
                    {
                    	
                    	
                        socket.receive(packet);
                        sockAddress = packet.getSocketAddress();
                        address = sockAddress.toString();

                        msg = new String(buf, 0, packet.getLength());

                        System.out.println(msg + "  received !!! by " + address);

                        //this case is UDP HolePunching reaction
//                        if (address.equals(HPaddress1))
//                        {
//                            System.out.println(msg + "hole punched");
//
//                            //So you can obtain own Global ip& port here. 
//                            //exchange this information 
//                            //`remoteHost` `remotePort` to another client 
//                            //with some method (signaling server)
//
//                        }
                    }
                    catch (IOException e) {
                    	System.out.println(e.getMessage());
                    }
                }

            }catch (Exception e){
            	System.out.println(e.getMessage());
            }
        }
    }

}
