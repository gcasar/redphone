package si.evil.redphone.activities;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify.IconValue;

import si.evil.redphone.GcmRegisterTask;
import si.evil.redphone.R;
import si.evil.redphone.data.ContactInfo;
import si.evil.redphone.fragments.ChatFragment;
import si.evil.redphone.fragments.ChatListFragment;
import si.evil.redphone.fragments.OverviewFragment;
import si.evil.redphone.provider.ChatListProvider;
import si.evil.redphone.provider.ContactProvider;
import si.evil.redphone.util.NFCUtils;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcAdapter.CreateNdefMessageCallback;
import android.nfc.NfcEvent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends FragmentActivity implements CreateNdefMessageCallback,NfcAdapter.OnNdefPushCompleteCallback{

	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	
	//HARDCODED, USE MSManager!
	public static final String REMOTE_URL = "http://bigbuckduck.com:9000/";
		
	
	public static final String STATE_FRAGMENT_KEY = "si.evil.redphone.FRAGMENT";
	public static final String STATE_CONTACT_KEY = "si.evil.redphone.CONTACT";
	public static final String MIME_CUSTOM = "application/si.evil.redphone.contact";

	private static final String PROPERTY_REG_ID = "si.evil.redphone.REG_ID";
	private static final String PROPERTY_APP_VERSION = "si.evil.redphone.APP_VER";

	public static final String SENDER_ID = "112750885366";
	
	ChatListFragment mChatFragment;
	OverviewFragment mOverviewFragment;
	
	ChatListProvider mChatListProvider;
	ContactProvider mContactProvider;
	
	GoogleCloudMessaging mGcm;
	
	NfcAdapter mNfcAdapter;
	
	String mActiveContactId;
	
	Toolbar mToolbar;
	ActionBar mActionBar;
	
	Handler mHandler;
	
	Menu mMenu;
	
	boolean restored = false;
	
	boolean isPaused = false;
	
	boolean mNfcEnabled = false;
	private boolean mNfcProcessed = true;
	private Intent mNfcIntent;
	
	private String mRegid = "";

	private Intent mNotifIntent;

	private String mOpenContactId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		

		mContactProvider = ContactProvider.getInstance(this);

		if(checkPlayServices()){
			mGcm = GoogleCloudMessaging.getInstance(this);
            mRegid = getRegistrationId(this);
            
            if (mRegid.isEmpty()) {
                registerInBackground();
            }
            
            if(mContactProvider.getOwnNumber()==null){
    			Intent i = new Intent(this, PairingActivity.class);
    			i.putExtra(PairingActivity.MODE_OWN, "true");
    			startActivity(i);
    		}else if(!mRegid.isEmpty()){
    			updateRemoteGCM(this, mRegid, mContactProvider.getOwnNumber());
    		}

            
		}
		
		
		
		
		
		//Setup nfc 
		mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
		mNfcEnabled = false;
		if(mNfcAdapter!=null){
			//mNfcAdapter.setNdefPushMessage(createNdefMessage(null), this);
	        mNfcAdapter.setNdefPushMessageCallback(this, this);
	        mNfcAdapter.setOnNdefPushCompleteCallback(this, this);
	        if(mNfcAdapter.isEnabled()){
	        	if(mNfcAdapter.isNdefPushEnabled()){
	        		mNfcEnabled = true;
	        	}
	        }
		}
		
		Intent intent = getIntent();
		if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {

	    	Log.e("NFC", "New NFC intent"+intent);
            mNfcIntent = intent;
        }
		
		

		mHandler = new Handler();
		mActionBar = getActionBar();
		restored = false;
		
		mChatListProvider = ChatListProvider.getInstance(this);
		
		System.out.println("onCreate");
		
		String fragment = null;
		if(savedInstanceState!=null)fragment = savedInstanceState.getString(STATE_FRAGMENT_KEY);
		if(savedInstanceState!=null)mActiveContactId = savedInstanceState.getString(STATE_CONTACT_KEY);
		
		
		String extra = intent.getStringExtra(STATE_CONTACT_KEY);
		if(extra!=null){
			mActiveContactId = extra;
			ContactInfo ci = mContactProvider.getById(mActiveContactId);
			if(ci!=null)mChatListProvider.open(ci);
			else{
				//try to open when contacts are loaded
				if(mContactProvider.isLoaded()==false){
					mOpenContactId = extra;
				}
			}
		}
		
	    if((fragment!=null&&fragment.compareToIgnoreCase(ChatFragment.class.getCanonicalName())==0)){
	    	
	    	openChatFragment(null, false);
	    }else{
	    	openContactsFragment(false);
	    }
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
	    // Save the user's current game state
	    savedInstanceState.putString(STATE_FRAGMENT_KEY, mChatFragment==null?OverviewFragment.class.getCanonicalName():ChatFragment.class.getCanonicalName());
	    savedInstanceState.putString(STATE_CONTACT_KEY, mActiveContactId);
	    
	    // Always call the superclass so it can save the view hierarchy state
	    super.onSaveInstanceState(savedInstanceState);
	}
	
	
	
	

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch(item.getItemId()){
		case R.id.action_pair:
			Intent i = new Intent(this, PairingActivity.class);
			i.putExtra(PairingActivity.MODE_OWN, "true");
			startActivity(i);
			break;
		}
		return super.onMenuItemSelected(featureId, item);
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		mMenu = menu;
		

		
		menu.findItem(R.id.action_pair).setIcon(
				new IconDrawable(this, IconValue.fa_retweet)
				.color(!mNfcEnabled?0xff777777:0xff333333)
				.actionBarSize()
				);

				
		return true;
	}

	public String getActiveContactId(){
		return mActiveContactId;
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	        if(mChatFragment!=null){
	        	openContactsFragment(true);
	        	return true;
	        }else{
	        	return super.onKeyDown(keyCode, event);
	        }
	    }
	    return super.onKeyDown(keyCode, event);
	}

	public void openChatFragment(ContactInfo ci, boolean animate) {
		if(isPaused)return;
		
		mActionBar.hide();
		
		if(ci!=null){
			mActiveContactId = ci.id;
			mChatListProvider.open(ci);
		}else{
			ci = mContactProvider.getById(mActiveContactId);
			if(ci!=null){
				mChatListProvider.open(ci);
			}
		}
		

		mChatFragment = new ChatListFragment();
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.setCustomAnimations(R.anim.up_in,R.anim.up_out);
		transaction.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
		if(!animate){
			transaction.setTransition(FragmentTransaction.TRANSIT_NONE);
		}
		transaction.replace(R.id.fragmentContainer, mChatFragment);
		transaction.commit();
		
	}
	
	public void openContactsFragment(boolean animate){
		if(isPaused)return;
		
		mChatFragment = null;
		mActionBar.show();
		if(mOverviewFragment==null){
				mOverviewFragment = new OverviewFragment();
		}
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.setCustomAnimations(R.anim.down_in,R.anim.down_out);
		transaction.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
		if(!animate){
			transaction.setTransition(FragmentTransaction.TRANSIT_NONE);
		}
		transaction.replace(R.id.fragmentContainer, mOverviewFragment);
		transaction.commit();
	}

	public void setCurrentCI(String id) {
		mActiveContactId = id;
	}

	@Override
	protected void onPause() {
		isPaused = true;
		super.onPause();
	}

	@Override
	protected void onResume() {
		isPaused = false;
		checkPlayServices();
		
		mNfcEnabled = false;
		if(mNfcAdapter!=null){
	        if(mNfcAdapter.isEnabled()){
	        	if(mNfcAdapter.isNdefPushEnabled()){
	        		mNfcEnabled = true;
	        	}
	        }
		}
		
		if(mMenu!=null){
			mMenu.findItem(R.id.action_pair).setIcon(
					new IconDrawable(this, IconValue.fa_retweet)
					.color(!mNfcEnabled?0xff777777:0xff333333)
					.actionBarSize()
					);
		}
		
		if(mNotifIntent!=null){
			openChatFragment(null, true);
			mNotifIntent = null;
		}
		

    	Log.e("NFC", "OnResume"+mNfcIntent);
		// Check to see that the Activity started due to an Android Beam
        if (mNfcIntent!=null&&NfcAdapter.ACTION_NDEF_DISCOVERED.equals(mNfcIntent.getAction())) {
            processIntent(mNfcIntent);
            mNfcIntent = null;
        }
		
		super.onResume();
	}
	
	@Override
    public void onNewIntent(Intent intent) {
        // onResume gets called after this to handle the intent
		
		String extra = intent.getStringExtra(STATE_CONTACT_KEY);
		if(extra!=null){
			mActiveContactId = extra;
			mNotifIntent = intent;
		}

    	Log.e("NFC", "New intent"+intent);
		if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {

	    	Log.e("NFC", "New NFC intent"+intent);
            mNfcIntent = intent;
        }
    }

    /**
     * Parses the NDEF Message from the intent and prints to the TextView
     */
    void processIntent(Intent intent) {
    	
    	Log.e("NFC", "Processing intent"+intent);
    	
        Parcelable[] rawMsgs = intent.getParcelableArrayExtra(
                NfcAdapter.EXTRA_NDEF_MESSAGES);
        // only one message sent during the beam
        final NdefMessage msg = (NdefMessage) rawMsgs[0];
        // record 0 contains the MIME type, record 1 is the AAR, if present
        //textView.setText(new String(msg.getRecords()[0].getPayload()));
        
        Intent nintent = new Intent(this, PairingActivity.class);
        nintent.putExtra(PairingActivity.PAYLOAD,new String( msg.getRecords()[0].getPayload()));
        startActivity(nintent);

    }
    
    public String getContactToOpen(){
    	return mOpenContactId;
    }

	@Override
    public NdefMessage createNdefMessage(NfcEvent event) {
		Log.e("NFC", "Sending");
		ContactInfo self = mContactProvider.getSelf();
		String text = "Hello!:080-ERROR:No data";
		if(self!=null){
//			if(self.oneTimeSecret!=null&&!self.oneTimeSecret.isEmpty()){
//				
//			}else{
//
//				self.oneTimeSecret = NFCUtils.generateOneTimeSecret();
//			}
			//secret is generated before this (in PairingActivity)
			text =NFCUtils.getPayloadFromContact(self);
		}
        final String data = text;


        NdefMessage msg = new NdefMessage(
                new NdefRecord[] { NFCUtils.supportCreateMime(
                        MIME_CUSTOM, text.getBytes())
         /**
          * The Android Application Record (AAR) is commented out. When a device
          * receives a push with an AAR in it, the application specified in the AAR
          * is guaranteed to run. The AAR overrides the tag dispatch system.
          * You can add it back in to guarantee that this
          * activity starts when receiving a beamed message. For now, this code
          * uses the tag dispatch system.
          */
          ,NdefRecord.createApplicationRecord("si.evil.redphone")
        });
        return msg;
    }

	@Override
	public void onNdefPushComplete(NfcEvent event) {
		Log.e("NFC", "Sent");
	}

	private boolean checkPlayServices() {
	    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
	    if (resultCode != ConnectionResult.SUCCESS) {
	        if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
	            GooglePlayServicesUtil.getErrorDialog(resultCode, this,
	                    PLAY_SERVICES_RESOLUTION_REQUEST).show();
	        } else {
	            Log.i("GCM", "This device is not supported.");
	            finish();
	        }
	        return false;
	    }
	    return true;
	}
	
	private static SharedPreferences getGCMPreferences(Context context) {
	    // This sample app persists the registration ID in shared preferences, but
	    // how you store the regID in your app is up to you.
	    return context.getSharedPreferences("si.evil.redphone.gcm",
	            Context.MODE_PRIVATE);
	}
	
	private static String getRegistrationId(Context context) {
	    final SharedPreferences prefs = getGCMPreferences(context);
	    String registrationId = prefs.getString(PROPERTY_REG_ID, "");
	    if (registrationId.isEmpty()) {
	        Log.i("GCM", "Registration not found.");
	        return "";
	    }
	    // Check if app was updated; if so, it must clear the registration ID
	    // since the existing regID is not guaranteed to work with the new
	    // app version.
	    int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
	    int currentVersion = getAppVersion(context);
	    if (registeredVersion != currentVersion) {
	        Log.i("GCM", "App version changed.");
	        return "";
	    }
	    return registrationId;
	}
	
	private static int getAppVersion(Context context) {
	    try {
	        PackageInfo packageInfo = context.getPackageManager()
	                .getPackageInfo(context.getPackageName(), 0);
	        return packageInfo.versionCode;
	    } catch (NameNotFoundException e) {
	        // should never happen
	        throw new RuntimeException("Could not get package name: " + e);
	    }
	}
	
	private void registerInBackground() {
	    GcmRegisterTask task = new GcmRegisterTask(this);
	    task.execute();
	}
	
	public static void updateRemoteGCM(Context c, String regid, String address){
		if(regid==null){
			regid = getRegistrationId(c);
		}
		 try {
			 JSONObject json = new JSONObject(
		        		"{'action':'register','gcmToken':'"+regid+"', 'address':'"+address+"'}"
		        		);
			 System.out.println(json.toString());
	            
				JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST,
				        REMOTE_URL, 
				        json,
				        new Response.Listener<JSONObject>() {
 
				            @Override
				            public void onResponse(JSONObject response) {
				                Log.d("GCM", response.toString());
				            }
				        }, new Response.ErrorListener() {
 
				            @Override
				            public void onErrorResponse(VolleyError error) {
				                Log.d("GCM", "Error: " + error.getMessage());
				            }
				        });
	            MainActivity.getRequestQueue(c).add(jsonObjReq);
			} catch (JSONException e) {

	            Log.e("GCM", "Error :" + e.getMessage());
			}

	}
	
	public void storeRegistrationId( String regId) {
		mRegid = regId;
	    final SharedPreferences prefs = getGCMPreferences(this);
	    int appVersion = getAppVersion(this);
	    Log.i("GCM", "Saving regId on app version " + appVersion);
	    SharedPreferences.Editor editor = prefs.edit();
	    editor.putString(PROPERTY_REG_ID, regId);
	    editor.putInt(PROPERTY_APP_VERSION, appVersion);
	    editor.commit();
	}
	
	static RequestQueue mRequestQueue;
	
	public  static RequestQueue getRequestQueue(Context c) {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(c);
        }
 
        return mRequestQueue;
    }

	public ContactInfo getSelf() {
		return mContactProvider.getSelf();
	}

	public void setContactToOpen(String object) {
		mOpenContactId = object;
		
	}

	public ContactInfo getActiveContact() {
		if(mActiveContactId!=null){
			return mContactProvider.getById(mActiveContactId);
		}
		return null;
	}
	
}
