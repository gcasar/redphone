package si.evil.redphone.activities;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;

import si.evil.redphone.R;
import si.evil.redphone.R.id;
import si.evil.redphone.R.layout;
import si.evil.redphone.R.menu;
import si.evil.redphone.data.ContactInfo;
import si.evil.redphone.provider.ContactProvider;
import si.evil.redphone.util.NFCUtils;
import si.evil.redphone.util.PhoneNumber;
import si.evil.redphone.util.SecureUtils;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class PairingActivity extends Activity {

	public static final String MODE_OWN = "si.evil.redbus.own";
	public static final String PAYLOAD = "si.evil.redbus.payload";

	
	boolean modeOwn = false;
	

	TextView vName;
	TextView vAdditional;
	
	TextView btnClose;
	
	EditText vPhoneNumber;
	
	ContactProvider provider;
	
	TextView vIcon;
	
	ImageView vPhoto;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		

		setContentView(R.layout.activity_pairing);
		

		vName = (TextView) findViewById(R.id.txtName);
		vAdditional = (TextView) findViewById(R.id.txtAdditional);
		
		vPhoneNumber = (EditText) findViewById(R.id.phone);
		
		btnClose = (TextView) findViewById(R.id.btnClose);
		vIcon = (TextView) findViewById(R.id.iconLocked);

		provider = ContactProvider.getInstance(this);
		vPhoto = (ImageView)findViewById(R.id.imgContact);
		
		
		Intent i = getIntent();
		if(i==null){
			this.finish();
			return;
		}
		if(i.hasExtra(MODE_OWN)){
			
			modeOwn = true;
			vName.setText(R.string.enter_number);
			vAdditional.setText(R.string.enter_number_additional);
			btnClose.setText("{fa-check}");
			
			//btnClose.setVisibility(View.INVISIBLE);
			ContactInfo self = provider.getSelf();
			if(self!=null&&self.id!=null){
				vPhoneNumber.setText(self.id);
			}
		}else{
			String data = i.getStringExtra(PAYLOAD);
			
			ContactInfo ci = NFCUtils.getContactFromData(data);
			boolean updated = false;
			if(ci.id!=null){
				ContactInfo existing = provider.getByPhone(ci.id);
				if(existing!=null){
					updated = true;
					existing.oneTimeSecret = ci.oneTimeSecret;
					ci = existing;
					provider.updateContactInfo(existing);
				}
			}
			
			btnClose.setText("{fa-check}");

			vName.setText(ci.name);
			vAdditional.setText(ci.id);
			
			if(ci.photoUri!=null){
				vPhoto.setImageURI(Uri.parse(ci.photoUri));
			}
			
			vPhoneNumber.setVisibility(View.GONE);
			vIcon.setVisibility(View.VISIBLE);
			
			if(updated==false){
				provider.createContact(ci);
			}
			
		}
		
		btnClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		
		
	}
	
	

	@Override
	protected void onDestroy() {
		if(modeOwn){
			//store phone number
			ContactInfo self = provider.getSelf();
			if(self!=null){
				self.id = PhoneNumber.simplify(vPhoneNumber.getText().toString());
				KeyPair kp;
				try {
					if(self.oneTimeSecret!=null&&!self.oneTimeSecret.isEmpty()){
						//update?
					}else{
						kp = SecureUtils.generateKey();
						
						self.oneTimeSecret = SecureUtils.encodePublic(kp.getPublic());
						self.secret = SecureUtils.encodePrivate(kp.getPrivate());
						
						String encoded = SecureUtils.RSAEncrypt("HELLO", self.oneTimeSecret);
						String decoded = SecureUtils.RSADecrypt(encoded, self.secret);
						
						System.out.println(decoded);
						
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					//Error generating secrets
				}
			}
			provider.storeSelf();

            MainActivity.updateRemoteGCM(this, null ,self.id);
		}
		super.onDestroy();
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pairing, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
