package si.evil.redphone.activities;

import java.net.InetAddress;
import java.net.UnknownHostException;

import si.evil.redphone.R;
import si.evil.redphone.StreamPlayer;
import si.evil.redphone.StreamRecorder;
import si.evil.redphone.R.id;
import si.evil.redphone.R.layout;
import si.evil.redphone.R.menu;
import si.evil.redphone.StreamRecorder.Callback;
import si.evil.redphone.network.Connection;
import si.evil.redphone.network.Connection.Callbacks;
import si.evil.redphone.network.UDPConnection;
import si.evil.redphone.network.UDPConnectionManager;
import si.evil.redphone.util.CircularBuffer;
import si.evil.redphone.util.Buffer;
import si.evil.redphone.util.Utils;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * @author gcasar
 *
 */
public class TestActivity extends Activity implements StreamRecorder.Callback, StreamPlayer.Callback, UDPConnection.Callbacks{

	protected static final String TAG = "PlaybackActivity";
	Button buttonPlay;
	Button buttonRecord;
	
	Button buttonConnect;
	Button buttonSend;
	
	InetAddress serverAddress;
	int serverPort = 8080;
	
	/**
	 * Source for text sending
	 */
	EditText textBar;
	EditText textPort;
	EditText textAddress;
	
	TextView labelLog;

	ProgressBar progressBar;
	ProgressBar writeBar;
	ProgressBar readBar;
	
	StreamRecorder recorder;
	
	StreamPlayer player;
	
	CircularBuffer buffer = new CircularBuffer(1440, 1000);
	
	UDPConnection connection;
	
	
	/**
	 * Used to message main thread
	 */
	Handler handler;

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test);
		
		//grab our handler so we can post messages to UI thread
		handler = new Handler();
		
		//delayed init so intro animation starts ASAP
		handler.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				delayedInit();
			}
		}, 100);	
	}
	
	void delayedInit(){
		setupMessagingTest();
		setupPlaybackTest();
	}
	
	void setupMessagingTest(){
		//UDP CONNECTION UI ELEMENTS
		buttonConnect = (Button) findViewById(R.id.connectButton);
		buttonSend = (Button) findViewById(R.id.sendButton);

		textAddress = (EditText) findViewById(R.id.address);
		textBar = (EditText) findViewById(R.id.textBox);
		textPort = (EditText) findViewById(R.id.port);
		
		labelLog = (TextView) findViewById(R.id.console);
		labelLog.setText("");
		prependLog("Internal IP:"+Utils.getIPAddress(true));

		AsyncTask<Void,Void,Void> task = new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void[] params) {
				try {
					serverAddress = InetAddress.getByName("bigbuckduck.com");
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
			}
		};
		task.execute();
		
		buttonConnect.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(connection!=null){
					connection.stop();//cleanup
				}
				int port = 8080;//default
				try{
					port = Integer.parseInt(textPort.getText().toString());
				}catch(NumberFormatException e){
					//invalid format
					textPort.setText(""+port);
				}
				//UDPConnectionManager.getInstance(TestActivity.this).punch(textAddress.getText().toString(), port, TestActivity.this);
				
			}
		});
		
		buttonSend.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(connection!=null){
					if(connection.send(new Buffer(textBar.getText().toString()))>0){
						prependLog(""+textBar.getText().toString());
						textBar.setText("");
					}else{
						prependLog("!!! failed to send. !!!");
					}
				}
			}
		});
	}
	
	void setupPlaybackTest(){
		//PLAYBACK/RECORD UI ELEMENTS
		buttonPlay = (Button) findViewById(R.id.playButton);
		buttonRecord = (Button) findViewById(R.id.recordButton);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		writeBar = (ProgressBar) findViewById(R.id.writeBar);
		readBar = (ProgressBar) findViewById(R.id.readBar);

		progressBar.setMax(buffer.getCapacity());
		writeBar.setMax(buffer.getCapacity());
		readBar.setMax(buffer.getCapacity());
		
		writeBar.getProgressDrawable().setColorFilter(Color.GREEN, Mode.SRC_IN);
		readBar.getProgressDrawable().setColorFilter(Color.YELLOW, Mode.SRC_IN);
		
		buttonRecord.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(buttonRecord.getText().toString().compareToIgnoreCase("REC")==0){
					recorder = new StreamRecorder(buffer);
					recorder.setCallback(TestActivity.this);
					recorder.start();
					buttonRecord.setText("STOP");
				}else{
					recorder.end();
					buttonRecord.setText("REC");
				}
			}
		});
		
		buttonPlay.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(buttonPlay.getText().toString().compareToIgnoreCase("PLAY")==0){
					player = new StreamPlayer(buffer);
					player.start();
					player.setCallback(TestActivity.this);
					buttonPlay.setText("STOP");
				}else{
					player.end();
					buttonPlay.setText("PLAY");
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.playback, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			buffer.reset();
			updateProgressBars();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		cleanupMessagingTest();
		cleanupPlaybackTest();
	}

	private void cleanupMessagingTest() {
		if(connection!=null){
			connection.stop();
		}
	}

	private void cleanupPlaybackTest() {
		buffer.release();
		UDPConnectionManager.getInstance(this).stop();
	}


	@Override
	public void onRecord(Buffer b) {
		
		handler.post(new Runnable() {
			
			@Override
			public void run() {
				updateProgressBars();
			}
		});
	}

	@Override
	public void onPlayFinished(Buffer b) {
		handler.post(new Runnable() {
			
			@Override
			public void run() {
				updateProgressBars();
			}
		});
	}
	
	class MsgHandler implements Runnable{
		String msg;
		
		public MsgHandler(String msg){
			this.msg = msg;
		}
		
		@Override
		public void run() {
			String current = labelLog.getText().toString();
			labelLog.setText(msg+"\n"+current);
		}
		
	}
	
	public void prependLog(String log){
		handler.post(new MsgHandler(log));
	}
	
	public void updateProgressBars(){
		progressBar.setProgress(buffer.getUnreadCount());
		writeBar.setProgress(buffer.getWritePosition());
		readBar.setProgress(buffer.getReadPosition());
	}

	@Override
	public void onRecv(Buffer msg, Connection con) {
	}
	
	@Override
	public void onError(Connection con, String code, String dsc) {
		prependLog("!!! Connection error !!!");
	}
	
	@Override
	public void onDsc(Connection con) {
		Log.i("DATA","onDsc");
	}
	
	@Override
	public void onConnect(Connection con) {
		Log.i("DATA","onConnect");
	}
	
	@Override
	public void onStateChange(Connection con, String prev, String now){
		Log.i("DATA","onStateChange:"+prev+"->"+now);
	}
	
//	@Override
//	public void onRecvRaw(Buffer raw, UDPConnection con) {
//		prependLog(">>"+raw.toString());
//	}
//
//	@Override
//	public void onBind(UDPConnection con) {
//		if(connection!=null){
//			connection.stop();
//		}
//		connection = con;
//		con.rawSend(new Buffer("Hello from a phone client!"), serverAddress, serverPort);
//		prependLog("Socket bound on port: "+con.getLocalPort());
//	}
}
