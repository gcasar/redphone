package si.evil.redphone.adapters;

import java.util.LinkedList;
import java.util.List;

import com.wilson.android.library.DrawableManager;

import si.evil.redphone.R;
import si.evil.redphone.activities.MainActivity;
import si.evil.redphone.activities.TestActivity;
import si.evil.redphone.data.ChatMessage;
import si.evil.redphone.data.ContactInfo;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ChatAdapter extends Adapter<ChatAdapter.Holder> {
	
	Activity context;

    List<ChatMessage> messages;
	
	public ChatAdapter(Activity activity){
		context = activity;
		messages = new LinkedList<ChatMessage>();
	}
	
	public void setData(List<ChatMessage> list){
		this.messages = list;
		this.notifyDataSetChanged();
	}
	


	@Override
	public int getItemCount() {
		return messages.size();
	}

	@Override
	public void onBindViewHolder(Holder holder, int i) {
	
		ChatMessage msg = messages.get(getItemCount()-i-1);
		if(!msg.remote){
			holder.right.setVisibility(View.VISIBLE);
			holder.left.setVisibility(View.GONE);
			
			holder.vTextRight.setText(msg.message);
			if(msg.system){
				holder.vIconRight.setVisibility(View.INVISIBLE);
				holder.vIconRight.setText("");
			}else{
				if(msg.secure){
					holder.vIconRight.setText("{fa-lock}");
				}else{
					holder.vIconRight.setText("{fa-unlock-alt}");
				}
				holder.vIconRight.setVisibility(View.VISIBLE);
			}
		}else{
			holder.left.setVisibility(View.VISIBLE);
			holder.right.setVisibility(View.GONE);
			
			holder.vTextLeft.setText(msg.message);
			if(msg.system){
				holder.vIconLeft.setVisibility(View.INVISIBLE);
				holder.vIconLeft.setText("");
			}else{
				if(msg.secure){
					holder.vIconLeft.setText("{fa-lock}");
				}else{
					holder.vIconLeft.setText("{fa-unlock-alt}");
				}
				holder.vIconLeft.setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
		
		View itemView = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_chat, viewGroup, false);


		return new Holder(itemView);
	}
	
	public static class Holder extends ViewHolder{
		 protected RelativeLayout left;
		 protected RelativeLayout right;
		 protected TextView vTextRight;
	     protected TextView vIconRight;
		 protected TextView vTextLeft;
	     protected TextView vIconLeft;

	     public Holder(View v) {
	          super(v);
	          right = (RelativeLayout)v.findViewById(R.id.right);
	          vTextRight =  (TextView) v.findViewById(R.id.txtRight);
	          vIconRight = (TextView)  v.findViewById(R.id.iconRight);
	          left = (RelativeLayout)v.findViewById(R.id.left);
	          vTextLeft =  (TextView) v.findViewById(R.id.txtLeft);
	          vIconLeft = (TextView)  v.findViewById(R.id.iconLeft);
	      }
		
	}
	

}
