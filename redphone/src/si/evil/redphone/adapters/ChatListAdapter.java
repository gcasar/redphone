package si.evil.redphone.adapters;

import java.util.List;

import si.evil.redphone.activities.MainActivity;
import si.evil.redphone.data.ContactInfo;
import si.evil.redphone.fragments.ChatFragment;
import si.evil.redphone.provider.ChatListProvider;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

public class ChatListAdapter  extends FragmentStatePagerAdapter implements ChatListProvider.Listener{
	
	Context context;
	ChatListProvider provider;
	
	List<ContactInfo> data;
	
	ChatFragment currentItem;
	
	public ChatListAdapter(Context c, FragmentManager fm) {
		super(fm);
		context = c;
		
		provider = ChatListProvider.getInstance(c);
		data = provider.getData();
		provider.setListener(this);
		
		
	}
	
	SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

	@Override
	public Fragment getItem(int i) {
		//todo: buffer!
		ChatFragment result = new ChatFragment();
		ContactInfo ci = data.get(i);
		System.out.println("Getting:"+ci.id+ " "+i);
		Bundle args = new Bundle();
		args.putString(MainActivity.STATE_CONTACT_KEY, ci.id);
		result.setArguments(args);
		currentItem = result;
		return result;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public void onDataUpdated(List<ContactInfo> n) {

		MainActivity main = ((MainActivity)context);
		String prevCi = main.getActiveContactId();
		int prev = data.size();
		ContactInfo ci = provider.getById(prevCi);
		data = provider.getData();
		this.notifyDataSetChanged();
		System.out.println("Data Updated");
		if(data.size()==0){
			main.setCurrentCI(null);
			main.openContactsFragment(true);
		}else{
			if(ci==null){
				//swap current
				ci = data.get(0);
				main.openChatFragment(ci, true);
			}else{
				main.openChatFragment(ci, true);
			}
		}
		
	}
	
	public ChatListProvider getProvider(){
		return provider;
	}

	


}
