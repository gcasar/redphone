package si.evil.redphone.adapters;

import java.util.List;

import com.wilson.android.library.DrawableManager;

import si.evil.redphone.R;
import si.evil.redphone.activities.MainActivity;
import si.evil.redphone.data.ContactInfo;
import android.app.Activity;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ContactsAdapter extends Adapter<ContactsAdapter.Holder> {
	
	DrawableManager iconManager;
	
	Activity context;

    List<ContactInfo> contactList;
	
	public ContactsAdapter(Activity activity, List<ContactInfo> list){
		iconManager = new DrawableManager(activity);
		context = activity;
		contactList = list;
	}
	
	public void setData(List<ContactInfo> list){
		int olds = contactList.size();
		this.contactList = list;
		//the following is buggy and crashes on some phones (GOOGLE±!!!)
		//this.notifyItemRangeInserted(olds-1, contactList.size()-1);
		this.notifyDataSetChanged();
	}

	@Override
	public int getItemCount() {
		return contactList.size();
	}

	@Override
	public void onBindViewHolder(Holder holder, int i) {
	
		
		final ContactInfo ci = contactList.get(i);
		if(ci.surname!=null&&!ci.surname.isEmpty()){
			holder.vName.setText(ci.name+" "+ci.surname);
		}else{
			holder.vName.setText(ci.name);
		}
        holder.vAdditional.setText(ci.id);

//		For some reason ImageLoader works horribly on contacts. (some phones ok)
//        String photoUri = ci.photoUri.substring(0, ci.photoUri.length()-6);
//        ImageLoader.getInstance().displayImage(photoUri, holder.vImage);
        holder.vImage.setImageResource(R.drawable.concact);
        if(ci.photoUri!=null)
        	iconManager.fetchDrawableOnThread(ci.photoUri, holder.vImage);

        //holder.vImage.setImageURI(Uri.parse(ci.photoUri));
        final Holder h = holder;
        holder.vg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((MainActivity)context).openChatFragment(ci,true);
			}
		});
	}

	@Override
	public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
		View itemView = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_contact, viewGroup, false);

		return new Holder(itemView);
	}
	
	public static class Holder extends ViewHolder{
		 protected RelativeLayout vg;
		 protected TextView vName;
	     protected TextView vAdditional;
	     protected ImageView vImage;

	     public Holder(View v) {
	          super(v);
	          vg = (RelativeLayout)v;
	          vImage = (ImageView) v.findViewById(R.id.imgContact);
	          vName =  (TextView) v.findViewById(R.id.txtName);
	          vAdditional = (TextView)  v.findViewById(R.id.txtAdditional);
	      }
		
	}
	
	public void cleanup(){
		iconManager.cleanup();
	}
	

}
