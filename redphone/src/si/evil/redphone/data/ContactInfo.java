package si.evil.redphone.data;

import java.io.IOException;
import java.io.Serializable;

public class ContactInfo implements Serializable, Comparable<ContactInfo> {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3337664390726316828L;
	
	
	public static final String NAME_PREFIX = "Name_";
    public static final String SURNAME_PREFIX = "Surname_";
    public static final String EMAIL_PREFIX = "email_";
	
    public String name;
    public String surname;
    public String email;
    
    public String id;
    
    
    public String photoUri;
    
    /**
     * In base64
     */
    public String secret;
    
    /**
     * In base64.
     * Used to send secret (it will be encrypted using this)
     */
    public String oneTimeSecret;
    
    
    private void writeObject(java.io.ObjectOutputStream out)
    	       throws IOException {
	     // write 'this' to 'out'...
    	out.writeBytes(name);
    	out.writeBytes(id);
    	out.writeBytes(id);
    	out.writeBytes(photoUri);
    	//TODO
    }
	   

   private void readObject(java.io.ObjectInputStream in)
       throws IOException, ClassNotFoundException {
     // populate the fields of 'this' from the data in 'in'...
	   //TODO
   }


	@Override
	public int compareTo(ContactInfo another) {
		return this.name.compareTo(another.name);
	}
}