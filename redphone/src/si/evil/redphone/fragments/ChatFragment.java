package si.evil.redphone.fragments;

import java.util.List;
import java.util.Random;

import si.evil.redphone.R;
import si.evil.redphone.activities.MainActivity;
import si.evil.redphone.adapters.ChatAdapter;
import si.evil.redphone.data.ChatMessage;
import si.evil.redphone.data.ContactInfo;
import si.evil.redphone.network.UDPConnectionManager;
import si.evil.redphone.provider.ChatListProvider;
import si.evil.redphone.provider.ContactProvider;
import si.evil.redphone.provider.ChatListProvider.ChatListener;
import si.evil.redphone.provider.ContactProvider.Listener;
import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Text chat
 * @author gcasar
 *
 */
public class ChatFragment extends Fragment implements ChatListener{
	
	protected boolean isPaused = false;
	
	ImageView vImage;
	TextView vName;
	TextView vAdditional;
	
	TextView btnClose;
	
	String mContactId;
	
	Handler mHandler;
	
	ContactInfo mContact;
	
	RecyclerView mChatList;
	
	ChatListProvider provider;
	
	ChatAdapter mChatAdapter;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		provider = ChatListProvider.getInstance(activity);
		mChatAdapter = new ChatAdapter(activity);
		mHandler = new Handler();
	}
	
	

	@Override
	public void setArguments(Bundle args) {
		mContactId = args.getString(MainActivity.STATE_CONTACT_KEY);

		System.out.println("setArgs"+mContactId);
		super.setArguments(args);
	}



	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		System.out.println("onCreate"+mContactId);
		if(savedInstanceState!=null){
			mContactId = savedInstanceState.getString(MainActivity.STATE_CONTACT_KEY);

			System.out.println("onCreateRestore:"+mContactId);
		}

		super.onCreate(savedInstanceState);
	}

	
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View result = inflater.inflate(R.layout.fragment_chat, null);
		
		vImage = (ImageView) result.findViewById(R.id.imgContact);
		vName = (TextView) result.findViewById(R.id.txtName);
		vAdditional = (TextView) result.findViewById(R.id.txtAdditional);
		

		btnClose = (TextView) result.findViewById(R.id.btnClose);
		btnClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ChatListProvider provider = ChatListProvider.getInstance(getActivity());
				provider.remove(mContactId);
			}
		});
		
		
		mChatList = (RecyclerView) result.findViewById(R.id.chatList);
		mChatList.setHasFixedSize(true);

		LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
		mChatList.setLayoutManager(mLayoutManager);
		
		mChatAdapter.setData(provider.getChatData(mContactId));
		mChatList.setAdapter(mChatAdapter);
		
		
		bindData();
		
		return result;
	}
	
	
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putString(MainActivity.STATE_CONTACT_KEY, mContactId);
		super.onSaveInstanceState(outState);
	}



	public void bindData(){
		System.out.println("FRAGbindData" + mContactId);

		MainActivity main = ((MainActivity)getActivity());
		
		
		
		
		ContactProvider provider = ContactProvider.getInstance(main);
		this.provider.setListener(this, mContactId);
		mContact = provider.getById(mContactId);
		
		ContactInfo ci = mContact;
		if(ci==null)return;
		
		Random rng = new Random(ci.id.hashCode());
		
		float[] hsv = new float[3];
	    hsv[0] = rng.nextInt(360);
	    hsv[1] = (float) 20/100f; // Some restrictions here?
	    hsv[2] = (float) 1;

	    mChatList.setBackgroundColor(Color.HSVToColor(hsv));
		
		
		if(ci.surname!=null&&!ci.surname.isEmpty()){
			vName.setText(ci.name+" "+ci.surname);
		}else{
			vName.setText(ci.name);
		}
        vAdditional.setText(ci.id);

//		For some reason ImageLoader works horribly on contacts. (some phones ok)
//        String photoUri = ci.photoUri.substring(0, ci.photoUri.length()-6);
//        ImageLoader.getInstance().displayImage(photoUri, holder.vImage);

        vImage.setImageResource(R.drawable.concact);
        if(ci.photoUri!=null)vImage.setImageURI(Uri.parse(ci.photoUri));
	}

	@Override
	public void onPause() {
		isPaused = true;
		super.onPause();
	}

	@Override
	public void onResume() {
		isPaused = false;
		super.onResume();
	}



	public void sendMessage(String string) {
		provider.send(string,mContactId);
	}



	@Override
	public void onDataUpdated(final List<ChatMessage> messages) {
		mHandler.post(new Runnable() {
			
			@Override
			public void run() {

				mChatAdapter.setData(messages);
			}
		});
	}


}
