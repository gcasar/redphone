package si.evil.redphone.fragments;

import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify.IconValue;

import si.evil.redphone.R;
import si.evil.redphone.activities.MainActivity;
import si.evil.redphone.adapters.ChatListAdapter;
import si.evil.redphone.data.ContactInfo;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Text chat
 * @author gcasar
 *
 */
public class ChatListFragment extends Fragment{
	
	protected boolean isPaused = false;
	
	EditText txtInput;
	TextView btnSend;
	
	ViewPager pager;
	ChatListAdapter adapter;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		
		MainActivity main = ((MainActivity)getActivity());

		adapter = new ChatListAdapter(activity, main.getSupportFragmentManager());
		
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View result = inflater.inflate(R.layout.fragment_chat_list, null);
		
		pager = (ViewPager)result.findViewById(R.id.pager);
		
		btnSend = (TextView)result.findViewById(R.id.btnSend);
		
		txtInput = (EditText)result.findViewById(R.id.input);
		
		btnSend.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment page = adapter.getRegisteredFragment(pager.getCurrentItem());
				if(page instanceof ChatFragment){
					ChatFragment f = (ChatFragment)page;
					f.sendMessage(txtInput.getText().toString());
					txtInput.setText("");
				}
				
			}
		});
		
		
		pager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				ContactInfo ci = adapter.getProvider().getByPos(arg0);
				if(ci!=null){

					MainActivity main = ((MainActivity)getActivity());
					main.setCurrentCI(ci.id);
				}
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		bindData();
		
		return result;
	}
	
	public void bindData(){
		System.out.println("bindData");

		
		MainActivity main = ((MainActivity)getActivity());
		int pos = adapter.getProvider().getPositionFromId(main.getActiveContactId());
		
		pager.setAdapter(adapter);
		pager.setCurrentItem(pos);
		
	}

	@Override
	public void onPause() {
		isPaused = true;
		super.onPause();
	}

	@Override
	public void onResume() {
		isPaused = false;
		super.onResume();
	}


}
