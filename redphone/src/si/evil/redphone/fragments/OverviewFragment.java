package si.evil.redphone.fragments;

import java.util.List;

import si.evil.redphone.R;
import si.evil.redphone.activities.MainActivity;
import si.evil.redphone.adapters.ContactsAdapter;
import si.evil.redphone.data.ContactInfo;
import si.evil.redphone.provider.ContactProvider;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Lists contacts
 * @author gcasar
 *
 */
public class OverviewFragment extends Fragment implements ContactProvider.Listener{
	
	protected boolean isPaused;
	
	ContactsAdapter mContactsAdapter;
	
	//DATA
	
	List<ContactInfo> mContacts;
	
	
	//VIEWS
	RecyclerView mContactsList;
	
	

	@Override
	public void onAttach(Activity activity) {
		
		ContactProvider contactsProvider = ContactProvider.getInstance(activity);
		contactsProvider.setListener(this);
		mContacts = contactsProvider.getData();
		

		mContactsAdapter = new ContactsAdapter(getActivity(),mContacts);

		
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		
		ViewGroup result = (ViewGroup)inflater.inflate(R.layout.fragment_overview, null);
		
		mContactsList = (RecyclerView) result.findViewById(R.id.contactsList);
		mContactsList.setHasFixedSize(true);

		LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
		mContactsList.setLayoutManager(mLayoutManager);
		
		mContactsList.setAdapter(mContactsAdapter);
		
		return result;
	}

	@Override
	public void onPause() {
		mContactsAdapter.cleanup();
		
		isPaused = true;
		super.onPause();
	}

	@Override
	public void onResume() {

		mContactsAdapter = new ContactsAdapter(getActivity(),mContacts);
		if(mContactsList!=null)mContactsList.setAdapter(mContactsAdapter);
		
		isPaused = false;
		super.onResume();
	}

	@Override
	public void onFinishedLoading(List<ContactInfo> data) {
		mContacts = data;
		mContactsAdapter.setData(mContacts);		
	}

	@Override
	public void onPartial(List<ContactInfo> partial, List<ContactInfo> full) {
		MainActivity activity = (MainActivity) getActivity();
		
		if(activity!=null&&activity.getContactToOpen()!=null){
			for(ContactInfo ci: partial){
				if(ci.id.equals(activity.getContactToOpen())){
					activity.openChatFragment(ci, true);
					activity.setContactToOpen(null);
					break;
				}
			}
		}
		mContacts = full;
		mContactsAdapter.setData(mContacts);		
	}

	@Override
	public void onError(List<ContactInfo> data) {
		// TODO Auto-generated method stub
		
	}
}
