package si.evil.redphone.network;

import java.util.LinkedList;

import si.evil.redphone.util.Buffer;

/**
 * Abstraction of a p2p connection</br>
 * Factory for connections.
 * @author gcasar
 *
 */
public abstract class Connection {
	
	Callbacks callbacks;
	
	/**
	 * Every call to a callback will be made in a background thread (with some exceptions).
	 * @param c
	 */
	public void setCallbacks(Callbacks c){
		this.callbacks = c;
	}
	

	/**
	 * Immutable preferences for connect method. Extended by Connection implementations to provide a method for 
	 * communicating connection parameters
	 * @author gcasar
	 *
	 */
	public static class Preferences{
		public static final String TYPE_ANY = "ANY";
		final String useType;
		
		public Preferences(){
			this(TYPE_ANY);
		}
		
		public Preferences(String type){
			useType = type;
		}
	}
	
	public interface Callbacks{
		void onRecv(Buffer msg, Connection con);
		/**
		 * For advanced monitoring. This may not include states such as {@link #onConnect(Connection)} 
		 * or other callbacks so do not use this for everything.<br>
		 * Values of prev and now are defined per {@link Connection} implementation.
		 * @param prev
		 * @param now
		 */
		void onStateChange(Connection con, String prev, String now);
		void onError(Connection con, String code, String desc);
		void onDsc(Connection con);
		void onConnect(Connection con);
	}
	
	public abstract int send(Buffer msg);
	
	/**
	 * Convenience method: same as calling {@link Connection#connect(String,Preferences)} witch prefs set to null
	 * @param address
	 * @return
	 */
	public static Connection connect(String address){
		return Connection.connect(address,(Connection.Preferences[])null);
	}
	
	/**
	 * TODO: implement preferences system
	 * @param address address hash for the wanted peer
	 * @param prefs array of preferences in order of prefered types of connections (if one fails next one will try to be established)
	 * @return
	 */
	public static Connection connect(String address, Preferences ... prefs){
		return null;
	}
	
	String id;
	
	public void setId(String id){
		this.id = id;
	}
	
	public String getId(){
		return id;
	}
}
