package si.evil.redphone.network;

import java.nio.charset.Charset;
import java.security.InvalidParameterException;

import android.support.annotation.NonNull;

import si.evil.redphone.util.Buffer;

/**
 * Simple UDP protocol packet. Every packet is at most 1440B<br>
 * 
 * <table>
 *  <tr>
 *  	<td>TYPE</td><td>ID</td><td>ACK</td>
 *  </tr>
 *  <tr>
 *  	<td>8b</td><td>32b</td><td>24b</td>
 *  </tr>
 * </table>
 * 
 * TYPE is used to quickly determine the packet type. See TYPE_* constants.<br>
 * 
 * ID is a 32bit sequential identifier. It is initialized to a random value at the start of comm<br>
 * 
 * ACK is 24bit bit offset to the last ack packet
 * 
 * 
 * @author gcasar
 *
 */
public class Packet {
	/**
	 * In Bytes
	 */
	public static final int HEADER_SIZE = 8;
	
	/**
	 * max payload size In Bytes
	 */
	public static final int PAYLOAD_MAX_SIZE = 1432;
	
	/**
	 * Used as a reply to SYN or as a single ACK to packets (with no body)
	 */
	public static final byte TYPE_ACK = 0x00;
	
	public static final byte TYPE_SYN = 0x01;
	
	/**
	 * When this packet is received it must be resent asap
	 */
	public static final byte TYPE_PING = 0x02;
	
	/**
	 * Packet that needs to be ACK-ed.
	 */
	public static final byte TYPE_MESSAGE = 0x0f;
	
	/**
	 * Messages of this type will not be resent if lost
	 */
	public static final byte TYPE_STREAM = 0x0e;
	
	Buffer buffer;
	
	byte type;
	
	int id;
	
	int ack;
	
	public Packet(Buffer internal) {
		buffer = internal;
	}
	
	public Packet(){
		buffer = new Buffer(1440);
	}
	
	/**
	 * Creates default {@link #TYPE_MESSAGE} with payload from another buffer
	 * @param payload anything greater than {@value #PAYLOAD_MAX_SIZE}
	 * @return
	 */
	public Packet fromBuffer(@NonNull Buffer payload)throws InvalidParameterException{
		final int size = payload.taken;
		if(size>PAYLOAD_MAX_SIZE){
			throw new InvalidParameterException("Payload size exceeds space ("+size+"/"+PAYLOAD_MAX_SIZE+")");
		}
		System.arraycopy(payload.data, 0, buffer.data, HEADER_SIZE, size);
		buffer.taken = HEADER_SIZE+size;
		return this;
	}
	
	public Packet fromString(@NonNull String payload)throws InvalidParameterException{
		final byte[] data = payload.getBytes(Charset.forName("UTF-8"));
		if(data.length>PAYLOAD_MAX_SIZE){
			throw new InvalidParameterException("Payload size exceeds space ("+data.length+"/"+PAYLOAD_MAX_SIZE+")");
		}
		System.arraycopy(data, 0, buffer.data, HEADER_SIZE, data.length);
		buffer.taken = HEADER_SIZE+data.length;
		return this;
	}
	
	public Packet setUnreliable(){
		this.type = TYPE_STREAM;
		return this;
	}
	
	/**
	 * This is the default ({@link #TYPE_MESSAGE}
	 * @return
	 */
	public Packet setReliable(){
		this.type = TYPE_MESSAGE;
		return this;
	}
	
	public Packet parseHeader(){
		if(buffer.taken>=HEADER_SIZE){
			type = buffer.data[0];
			id = 0;
			id = id | buffer.data[1]<<24;
			id = id | buffer.data[2]<<16;
			id = id | buffer.data[3]<<8;
			id = id | buffer.data[4];
			ack = 0;
			ack = ack | buffer.data[5]<<16;
			ack = ack | buffer.data[6]<<8;
			ack = ack | buffer.data[7];
		}
		return this;
	}

	public byte getType() {
		return type;
	}

	public int getId() {
		return id;
	}
	
	

}
