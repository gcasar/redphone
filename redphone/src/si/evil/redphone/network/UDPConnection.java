package si.evil.redphone.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import si.evil.redphone.network.UDPConnectionManager.Message;
import si.evil.redphone.util.Buffer;
import si.evil.redphone.util.CircularBuffer;

/**
 * A simple point to point protocol over udp. Every connection uses an unique port
 * This type of connection is able to receive from two addresses: server and the peer
 * @author gcasar
 *
 */
public class UDPConnection extends Connection{
	
	public enum GlobalState{
		DISCONNECTED, 
		STARTING,
		PUNCHING, //eg: connecting
		CONNECTED,
		ERROR
	}
	
	GlobalState state = GlobalState.DISCONNECTED;
	
	/**
	 * One of {@link Packet#TYPE_MESSAGE or Packet#TYPE_STREAM}.<br>
	 * New payload packets will be formed as this mode
	 */
	int mode = Packet.TYPE_MESSAGE;
	
	/**
	 * Created at the start of worker thread
	 */
	DatagramSocket socket;
	
	/**
	 * Peer address
	 */
	InetAddress address;
	
	/**
	 * Peer address
	 */
	String rawAddress;
	
	/**
	 * Local address
	 */
	String localAddress;
	
	/**
	 * Currently active punch server
	 */
	InetAddress serverAddress;

	int peerPort;
	
	int serverPort;
	
	/**
	 * Will be set to a number between {@link UDPConnectionManager#DEFAULT_PORT_RANGE_START} and {@ UDPConnectionManager#DEFAULT_PORT_RANGE_END}
	 */
	int localPort;
	
	/**
	 * Counter serves as a statistic and as a default priority parameter
	 */
	int totalMessagesSent = 0;
	
	/**
	 * Number of packets with payload (non-control ones). Also counts payloads sent via piggy-backing
	 */
	int totalPayloadsSent = 0;
	
	/**
	 * Datagram buffer, used by worker
	 */
	DatagramPacket dgramPacket;
	
	/**
	 * CircularBuffer for payloads. If this is not null than payloads will also be fed into this
	 */
	CircularBuffer directBuffer;
	
	
	/**
	 * Next active payload buffer (taken from {@link #directBuffer}). Can be null
	 */
	Buffer activeBuffer;
	
	
	/**
	 * indicates if the loop thread should continue. Set by {@link #start()} and {@link #stop()}
	 */
	boolean isRunning;
	
	Worker worker;
	
	UDPConnectionManager manager;
	

	/**
	 * Creates the socket
	 * 
	 * @param address
	 * @param port
	 * @param cbck must not be null
	 * @throws SocketException 
	 */
	public UDPConnection(String local, String address, si.evil.redphone.network.Connection.Callbacks cbck, UDPConnectionManager manager) {
		this.rawAddress = address;
		this.localAddress = local;
		this.callbacks = cbck;
		
		this.manager = manager;
		
		worker = new Worker();
		
	}
	
	/**
	 * If no direct buffer is set than all messages will be fed into new objects
	 * @param b
	 */
	public synchronized void setDirectBuffer(CircularBuffer b){
		this.directBuffer = b;
	}

	/**
	 * Tries to immediately send the message. Usually called from {@link UDPConnectionManager#worker} thread
	 */
	int doRawSend(Buffer msg, InetAddress address, int port) {
		if(socket!=null){
			if(address == null)address = this.address;
			if(port<0)port = this.peerPort;
            DatagramPacket packet = new DatagramPacket(msg.data, msg.taken, address, port);
            try {
				socket.send(packet);
				return msg.taken;
			} catch (IOException e) {
				e.printStackTrace();
				return -1;
			} catch (Exception e){
				e.printStackTrace();
				return -1;
			}
            
		}else return 0;
	}
	
	public void setModeUnreliable(){
		this.mode=Packet.TYPE_STREAM;
	}
	
	public void setModeReliable(){
		this.mode=Packet.TYPE_MESSAGE;
	}
	
	/**
	 * Returns >0 on success and 0 if socket is not initialized.<br>
	 * Constructs a Packet based on current mode (either {@link Packet#TYPE_MESSAGE}
	 * or {@link Packet#TYPE_STREAM}) or drops the message if we are not connected to 
	 * peer.
	 */
	@Override
	public int send(Buffer msg){
		//todo
		if(socket!=null&&msg!=null){
			totalMessagesSent++;
			manager.msgQueue.add(new Message(this,msg,totalMessagesSent));
			return 1;
		}
		return 0;
	}

	/**
	 * Will add the message to message queue regardless of state. Use {@link #send(Buffer)} instead
	 * @param msg
	 * @param address
	 * @param port
	 * @return
	 */
	public int rawSend(Buffer msg, InetAddress address, int port){
		if(socket!=null&&msg!=null){
			totalMessagesSent++;
			manager.msgQueue.add(new Message(this,msg,address,port,totalMessagesSent));
			return 1;
		}
		return 0;
	}
	
	//TODO
	public void start(){
		isRunning = true;
		worker.start();
	}
	
	//TODO
	/**
	 * Will stop the recv loop and unbind the socket
	 */
	public void stop(){
		isRunning = false;
	}
	
	/**
	 * This method is called from Worker thread. It handles protocol
	 * @param buffer
	 */
	public void onRecv(Buffer buffer) {
		if(callbacks!=null)callbacks.onRecv(buffer, this);
	}
	
	/**
	 * Releases socket (unbinds, closes)
	 */
	public void release(){
		if(socket!=null){
			socket.close();
			socket = null;
		}
	}
	
	//Message makers
	public static Buffer makeMsgAddr(int id,String localIp, int localPort, String address, String local, Buffer buf){

		JSONObject json = new JSONObject();
		try {
			json.put("id", id);
			json.put("msg", "ADDR");
			json.put("localIp", localIp);
			json.put("localPort", localPort);
			json.put("address", local);
			json.put("target", address);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String payload = json.toString();
		
		Log.d("UDP","Making msg: "+payload);
		if(buf==null){
			buf = new Buffer(payload);
		}else{
			buf.fromString(payload);
		}
		
		return buf;
	}
	
	public Buffer makePunchMsg(int id, Buffer buf){
		JSONObject json = new JSONObject();
		try {
			json.put("id", id);
			json.put("hello", "hello");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String payload = json.toString();
		
		Log.d("UDP","Making msg: "+payload);
		if(buf==null){
			buf = new Buffer(payload);
		}else{
			buf.fromString(payload);
		}
		
		return buf;
	}
	
	//State handlers. Used in Worker#run
	
	/**
	 * Grabs MServer info and initates request with the wanted {@link #address}<br>
	 * Info sent to MServer:
	 * -local ip
	 * -local port
	 * -(TODO)
	 */
	private void _starting(){
		this.serverAddress = manager.getMServerAddress();
		this.serverPort = manager.getMServerPort();
		
		//send ADDR message to Server
		Buffer addr = makeMsgAddr(totalPayloadsSent,manager.getLocalIp(), localPort, rawAddress, localAddress, null);
		rawSend(addr, serverAddress,serverPort);
	}
	
	private void _connected(){
		//get data packets?
	}
	
	private void _punching(){
		//start sending packets to destination, waiting for ack

		//send ADDR message to Server
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			//might as well
		}
		setLog("Punch"+totalPayloadsSent);
		Buffer addr = makePunchMsg(totalPayloadsSent,null);
		rawSend(addr, address,peerPort);
	}
	private void _starting(DatagramPacket packet){
		
		SocketAddress senderAddress = null;

		Log.d("UDP","Msg:"+activeBuffer.toString());
		senderAddress=packet.getSocketAddress();
		//TODO CHECK SENDER
		if(this.serverAddress.equals(serverAddress)){
			//data recieved
			try {
				JSONObject json = new JSONObject(activeBuffer.toString());
				String remoteIp = json.getString("publicIp");
				int remotePort = json.getInt("publicPort");
				String localIp = json.getString("localIp");
				int localPort = json.getInt("localPort");
				
				if(remoteIp.equals(localIp)){
					setLog("devices behind same NAT");
					remotePort = localPort;
				}

				Log.d("UDP","Set remote to "+remoteIp+" "+remotePort);
				Log.d("UDP","Set local to "+localIp+" "+localPort);
				
				
				
				this.peerPort = remotePort;
				this.address = InetAddress.getByName(remoteIp);
				
				//TODO: check local
				setState(GlobalState.PUNCHING,null);
				
			} catch (JSONException e) {
				

				setState(GlobalState.ERROR,"Bad format: "+activeBuffer.toString());
				e.printStackTrace();
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			Log.d("UDP","Ignoring message:"+activeBuffer.toString());
		}
	}
	
	private void _connected(DatagramPacket packet){
		if(callbacks!=null)callbacks.onRecv(activeBuffer, this);
	}
	
	private void _punching(DatagramPacket packet){
		SocketAddress senderAddress = null;
		senderAddress=packet.getSocketAddress();
		if(!senderAddress.equals(serverAddress)){
			//data recieved

			setState(GlobalState.CONNECTED,null);

		}else{
			Log.d("UDP","Ignoring message:"+activeBuffer.toString());
		}
	}
	
	private void setState(GlobalState ns, String desc){


		Log.d("UDP","Upgrading state: "+ns);
		if(this.callbacks!=null)callbacks.onStateChange(this, state.toString(), ns.toString());
		switch(ns){
		case STARTING:
			break;
		case CONNECTED:
			if(this.callbacks!=null)callbacks.onConnect(this);
			break;
		case PUNCHING:
			break;
		case DISCONNECTED:

			if(this.callbacks!=null)callbacks.onDsc(this);
			break;
		case ERROR:
			if(this.callbacks!=null)callbacks.onError(this,ns.toString(),desc);
			break;
		}
		
		state = ns;
	}
	
	private void setLog(String log){
		if(this.callbacks!=null)callbacks.onStateChange(this, "LOG",log);
	}
	
	
	protected DatagramPacket recv(DatagramPacket packet){
		try {
			socket.receive(packet);
		} catch (IOException e) {
			//TODO: a more gracefull error handling
			e.printStackTrace();

			setState(GlobalState.ERROR,e.getMessage());
			isRunning = false;
		}
		activeBuffer.taken = packet.getLength();
		return packet;
		
	}
	
	/**
	 * Socket loop
	 * @author gcasar
	 *
	 */
	class Worker extends Thread{
		@Override
		public void run(){

			socket = manager.setupSocket();
			localPort = socket.getLocalPort();
			
			if(socket==null){
				isRunning = false;
				state = UDPConnection.GlobalState.ERROR;
				return; //illegal state
			}
			

			setState(GlobalState.STARTING,null);
			
			SocketAddress senderAddress = null;
			activeBuffer = new Buffer(1440);

	        DatagramPacket packet = new DatagramPacket(activeBuffer.data,activeBuffer.data.length);
			
			
			while(isRunning){
				switch(state){
				case STARTING:
					_starting();
					break;
				case CONNECTED:
					_connected();
					break;
				case PUNCHING:
					_punching();
					break;
				case DISCONNECTED:
				case ERROR:
				default:
					return;
				}
				
				recv(packet);
				
				switch(state){
				case STARTING:
					_starting(packet);
					break;
				case CONNECTED:
					_connected(packet);
					break;
				case PUNCHING:
					_punching(packet);
					break;
				case DISCONNECTED:
				case ERROR:
				default:
					return;
				}
				
				
			}


			

			
			if(callbacks!=null)callbacks.onConnect(UDPConnection.this);

			
			
            release();
                
		}
		
	}



	public int getLocalPort() {
		return localPort;
	}

	public void disconnect() {
		if(state!=GlobalState.DISCONNECTED){
			setState(GlobalState.DISCONNECTED,null);
		}
		
	}

}
