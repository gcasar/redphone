package si.evil.redphone.network;

import si.evil.redphone.MSManager;
import si.evil.redphone.network.Connection;
import si.evil.redphone.network.Connection.Callbacks;
import si.evil.redphone.util.Buffer;
import si.evil.redphone.util.Utils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;

/**
 * Manages UDP sockets and creates virtual connections between pairs of addresses (us and them).</br>
 * 
 * Creates a thread for every listening socket (every port).
 * Every connection is restricted to one port (simplicity)<br>
 * 
 * So only one socket is supported (using default port)<br>
 * 
 * @author gcasar
 *
 */
public class UDPConnectionManager {
	
	public static UDPConnectionManager instance;
	
	public static UDPConnectionManager getInstance(Context con){
		if(instance==null){
			instance = new UDPConnectionManager(con);
		}
		return instance;
	}
	
	/**
	 * Random port - first connection will default to this one
	 */
	public static final int DEFAULT_PORT = 9999;
	
	/**
	 * Unless set otherwise all sockets will randomly choose a port between these two values.
	 */
	public static final int DEFAULT_PORT_RANGE_START = 10000;
	
	public static final int DEFAULT_PORT_RANGE_END = 11111;
	
	/**
	 * {@link #setupSocket()} will stop after the following constant
	 */
	public static final int MAX_BIND_RETRY = 100;
	
	protected int lastPort = DEFAULT_PORT;
	
	
	
	/**
	 * sending thread for all UDPConnections
	 */
	Worker worker = new Worker();
	
	/**
	 * Organized by address
	 */
	HashMap<String, UDPConnection> mAdressConn = new HashMap<String, UDPConnection>();
	
	static class Message implements Comparable<Message>{
		UDPConnection connection;
		Buffer message;
		InetAddress address;
		int port = -1;
		int priority;
		
		public Message(){
			
		}
		
		public Message(UDPConnection c, Buffer m, int priority){
			message = m;
			connection = c;
			this.priority = priority;
		}
		
		public Message(UDPConnection c, Buffer m, InetAddress address, int port, int priority){
			message = m;
			connection = c;
			this.address = address;
			this.port = port;
			this.priority = priority;
		}

		@Override
		public int compareTo(Message another) {
			return this.priority-another.priority;
		}
	}
	
	/**
	 * Used to signal worker thread to stop
	 */
	static Message SHUTDOWN_MESSAGE = new Message();
	
	BlockingQueue<Message> msgQueue = new PriorityBlockingQueue<UDPConnectionManager.Message>();

	public boolean workerRunning = false;
	
	Context context;
	
	String localIp;
	
	String localPort;
	
	public UDPConnectionManager(Context con){
		start();
	}
	
	/**
	 * Binds the socket to the next valid address
	 * @return a bound socket or null if retry exceeded {@link #MAX_BIND_RETRY} ({@value #MAX_BIND_RETRY})
	 */
	public synchronized DatagramSocket setupSocket(){
		for(int i=0; i<MAX_BIND_RETRY; i++){

			
			try {
				DatagramSocket result = new DatagramSocket(lastPort);
				return result;
			} catch (SocketException e) {
				//continue to increment port

				if(++lastPort>DEFAULT_PORT_RANGE_END)
					lastPort = DEFAULT_PORT_RANGE_START;
			}
		}
		return null;
	}

	public InetAddress getMServerAddress(){
		return MSManager.getInstance().getActiveServerAddress();
	}
	
	public int getMServerPort(){
		return MSManager.getInstance().getActiveServerPort();
	}
	
	/**
	 * starts worker thread for sending messages
	 */
	void start(){
		workerRunning = true;
		worker.start();
	}
	
	/**
	 * Signals the sender thread to stop. It is started only once so be sure this is the last thing you do before exiting the app
	 */
	public void stop(){
		msgQueue.clear();
		msgQueue.add(SHUTDOWN_MESSAGE);
	}
	
	/**
	 * Sender thread
	 * @author gcasar
	 *
	 */
	class Worker extends Thread{
		@Override
        public void run()
        {	
			Message msg;
            try {
				while((msg = msgQueue.take())!=SHUTDOWN_MESSAGE){
					if(msg.connection!=null){
						msg.connection.doRawSend(msg.message, msg.address, msg.port);
					}
				}
			} catch (InterruptedException e) {
				workerRunning = false;
			}
            workerRunning = false;
        }
	}

	public String getLocalIp() {
		if(localIp==null){
			localIp = Utils.getIPAddress(true);
		}
		return localIp;
	}

	
	
}
