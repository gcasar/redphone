package si.evil.redphone.provider;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import si.evil.redphone.data.ChatMessage;
import si.evil.redphone.data.ContactInfo;
import si.evil.redphone.network.Connection;
import si.evil.redphone.network.Connection.Callbacks;
import si.evil.redphone.network.UDPConnection;
import si.evil.redphone.network.UDPConnectionManager;
import si.evil.redphone.util.Buffer;
import si.evil.redphone.util.SecureUtils;

import android.content.Context;

public class ChatListProvider implements Callbacks {
	
	public static interface Listener{
		void onDataUpdated(List<ContactInfo> data);
	}
	
	public static interface ChatListener{
		void onDataUpdated(List<ChatMessage> messages);
	}
	
	
	
	static ChatListProvider instance;
	
	
	
	UDPConnectionManager manager;
	
	ContactProvider contactProvider;
	
	public static ChatListProvider getInstance(Context c){
		if(instance==null){
			instance=new ChatListProvider(c);
		}
		return instance;
	}
	
	Context context;
	
	List<ContactInfo> data;

	HashMap<String, ContactInfo> contacts = new HashMap<String, ContactInfo>();
	HashMap<String, UDPConnection> connections = new HashMap<String, UDPConnection>();
	
	HashMap<String, LinkedList<ChatMessage>> chats = new HashMap<String, LinkedList<ChatMessage>>();
	HashMap<String, WeakReference<ChatListener>> listeners = new HashMap<String, WeakReference<ChatListener>>();
	
	
	//TODO: weakreference
	Listener listener;
	
	protected ChatListProvider(Context c) {
		context = c;
		manager = UDPConnectionManager.getInstance(c);
        contactProvider = ContactProvider.getInstance(c);
        data = new LinkedList<ContactInfo>();
        
	}
	
	public List<ContactInfo> getData(){
		return data;
	}
	
	public void setListener(Listener l){
		this.listener = l;
	}
	

	//TODO: add hashmap
	public ContactInfo getById(String id) {
		for(ContactInfo ci: data){
			if(ci.id.equals(id)){
				return ci;
			}
		}
		return null;
	}

	public void open(ContactInfo ci) {
		//TODO: better structures
		if(data.contains(ci)==false){
			data.add(ci);
			contacts.put(ci.id, ci);
			ContactInfo self = contactProvider.getSelf();
			UDPConnection con = new UDPConnection(self.id, ci.id, this, manager);
			con.start();
			con.setId(ci.id);
			connections.put(ci.id, con);
			chats.put(ci.id, new LinkedList<ChatMessage>());
			if(listener!=null)listener.onDataUpdated(data);
		}
	}
	
	public void setListener(ChatListener l, String id){
		listeners.put(id, new WeakReference<ChatListProvider.ChatListener>(l));
	}
	
	//TODO:structures!
	public int getPositionFromId(String id){
		int i=0;
		for(ContactInfo ci: data){
			if(ci.id.equals(id)){//compare by ref is intentional
				return i;
			}
			i++;
		}
		return 0;
	}

	public ContactInfo getByPos(int pos) {
		if(pos>=0&&pos<data.size()){
			return data.get(pos);
		}
		return null;
	}

	public void remove(String mContactId) {
		int i= 0;
		for(ContactInfo ci: data){
			if(ci.id.equals(mContactId)){//compare by ref is intentional
				break;
			}
			i++;
		}
		if(i<data.size()){
			data.remove(i);
			contacts.remove(mContactId);
			chats.remove(mContactId);
			UDPConnection con = connections.remove(mContactId);
			con.disconnect();
			con.stop();
			if(listener!=null)listener.onDataUpdated(data);
			
		}
	}
	
	
	public ChatListener getListener(String id){
		WeakReference<ChatListener> wr = listeners.get(id);
		if(wr==null){
			listeners.remove(id);
			return null;
		}
		ChatListener l = wr.get();
		if(l==null){
			listeners.remove(id);
		}
		return l;
	}

	@Override
	public void onRecv(Buffer msg, Connection con) {
		LinkedList<ChatMessage> msgs = chats.get(con.getId());
		ChatListener l = getListener(con.getId());
		if(msgs!=null){
			ChatMessage nmsg = new ChatMessage();
			String raw = msg.toString();
			if(raw.startsWith("#")){
				//endoded
				nmsg.secure = true;
				raw = raw.substring(1);
				ContactInfo self = contactProvider.getSelf();
				try {
					raw = SecureUtils.RSADecrypt(raw, self.secret);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					raw = "Error while decrypting";
				} 
			}else{
				raw = raw.substring(1);
				nmsg.secure = false;
			}
			nmsg.message = raw;
			nmsg.remote = true;
			nmsg.system = false;
			msgs.add(nmsg);
			if(l!=null){
				l.onDataUpdated(msgs);
			}
		}
	}

	@Override
	public void onStateChange(Connection con, String prev, String now) {
		LinkedList<ChatMessage> msgs = chats.get(con.getId());
		ChatListener l = getListener(con.getId());
		if(msgs!=null){
			ChatMessage nmsg = new ChatMessage();
			nmsg.message = "State change: "+prev+"->"+now;
			nmsg.remote = false;
			nmsg.system = true;
			nmsg.secure = false;
			msgs.add(nmsg);
			if(l!=null){
				l.onDataUpdated(msgs);
			}
		}
	}

	@Override
	public void onError(Connection con, String code, String desc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDsc(Connection con) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnect(Connection con) {
		// TODO Auto-generated method stub
		
	}

	public void send(String string, String mContactId) {
		UDPConnection con = connections.get(mContactId);
		ChatListener l = getListener(mContactId);
		
		ContactInfo ci = contacts.get(mContactId);
		
		if(con!=null){
			
			LinkedList<ChatMessage> msgs = chats.get(mContactId);

			ChatMessage nmsg = new ChatMessage();
			nmsg.remote = false;

			nmsg.message = string;
			nmsg.system = false;
			if(ci.oneTimeSecret!=null&&ci.oneTimeSecret.isEmpty()){
				string = "!"+string;
				nmsg.secure = false;
			}else{
				try {
					string = "#"+SecureUtils.RSAEncrypt(string, ci.oneTimeSecret);

					nmsg.secure = true;
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					string = "!"+string;
					nmsg.secure = false;
					e.printStackTrace();
				}
			}
			

			con.send(new Buffer(string));
			

			msgs.add(nmsg);
			if(l!=null)l.onDataUpdated(msgs);
		}else{
			LinkedList<ChatMessage> msgs = chats.get(mContactId);
			ChatMessage nmsg = new ChatMessage();
			nmsg.message = "Failed to send: no connection";
			nmsg.remote = false;
			nmsg.system = true;
			nmsg.secure = false;
			msgs.add(nmsg);
			if(l!=null)l.onDataUpdated(msgs);
		}
	}

	public List<ChatMessage> getChatData(String mContactId) {
		if(chats.containsKey(mContactId))
			return chats.get(mContactId);
		else return new LinkedList<ChatMessage>();
	}
}
