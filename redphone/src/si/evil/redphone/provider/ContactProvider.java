package si.evil.redphone.provider;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import si.evil.redphone.data.ContactInfo;
import si.evil.redphone.util.PhoneNumber;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.Log;

public class ContactProvider {
	

	public static final String SHARED_PREFS = "si.evil.redphone.CONTACTS";

	public static final String CONTACTS_KEY = "si.evil.redphone.contacts_list";

	public static final String CONTACT_SECRET = "csecret_";
	public static final String CONTACT_PHONE = "cphone_";
	public static final String CONTACT_NAME = "cname_";

	public static final String PHONE_NUMBER = "own_number";
	public static final String PHONE_PRIVATE = "own_private";
	public static final String PHONE_PUBLIC = "own_public";
	
	
	public static interface Listener{
		void onFinishedLoading(List<ContactInfo> data);
		
		void onPartial(List<ContactInfo> partial, List<ContactInfo> full);
		
		void onError(List<ContactInfo> data);
	}
	
	
	
	static ContactProvider instance;
	
	public static ContactProvider getInstance(Context c){
		if(instance==null){
			instance=new ContactProvider(c);
		}
		return instance;
	}
	
	Context context;
	
	ContactInfo self;
	
	List<ContactInfo> data;
	
	boolean loaded;
	
	HashMap<String,ContactInfo> contactIds;
	
	//TODO: weakreference
	Listener listener;
	
	protected ContactProvider(Context c) {
		context = c;
		
        contactIds = new HashMap<String,ContactInfo>();
        data = new LinkedList<ContactInfo>();
        
        
        //load data about self asap
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
		

		TelephonyManager tMgr = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		self = new ContactInfo();

		self.secret = prefs.getString(PHONE_PRIVATE, null);
		self.oneTimeSecret = prefs.getString(PHONE_PUBLIC, null);

		String number = prefs.getString(PHONE_NUMBER, null);
		if(number!=null)self.id = number;
		else{
			number = tMgr.getLine1Number();
			if(number!=null){
				self.id = PhoneNumber.simplify(number);
			}
		}
		AccountManager manager = AccountManager.get(context); 
	    Account[] accounts = manager.getAccountsByType("com.google"); 

	    for (Account account : accounts) {
	      self.name=account.name;
	      break;
	    }

	    Log.e("SELF", self.name+" "+self.id+" "+self.id);

		
        
        //TODO: config changes!!
        new Worker().execute();
	}
	
	public List<ContactInfo> getData(){
		return data;
	}
	
	public void setListener(Listener l){
		this.listener = l;
	}
	
	protected class Worker extends AsyncTask<Void, ContactInfo, List<ContactInfo>>{


		@Override
		protected List<ContactInfo> doInBackground(Void... params) {
			HashMap<String,ContactInfo> indexed = new HashMap<String,ContactInfo>();
			
			

			//load from preferences
			SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
			

			
			Set<String> contacts = prefs.getStringSet(CONTACTS_KEY, new HashSet<String>());
			for(String key: contacts){
				ContactInfo ci = new ContactInfo();
				ci.name = prefs.getString(CONTACT_NAME+key, "Anonymous");
				ci.id = prefs.getString(CONTACT_PHONE+key, "080-MISSING");
				
				//public key
				ci.oneTimeSecret = prefs.getString(CONTACT_SECRET+key, "");
				ci.id = key;
				indexed.put(ci.id, ci);
				publishProgress(ci);
			}
			

			ContentResolver cr = context.getContentResolver();
	        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
	                null, ContactsContract.Contacts.DISPLAY_NAME_PRIMARY+ " IS NOT NULL",
	                null, ContactsContract.Contacts.DISPLAY_NAME_PRIMARY + " ASC");
	        
	        int pos = 0;
	        if (cur.getCount() > 0) {
	            while (cur.moveToNext()) {
	            	pos ++;
	            	//populate contact info
	            	
	            	ContactInfo ci = new ContactInfo();
	            	
	            	
	            	ci.id = null;
	            	String id =cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
	                ci.name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY));
	                ci.email = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_ALTERNATIVE));
	                ci.photoUri = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI));
	       
	                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
	                	Cursor pCur = cr.query(
	                			ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
	                            null,
	                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
	                            new String[]{id}, 
	                            null
	                            );
	                	
						while (pCur.moveToNext()) {
						    ci.id = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
						    if(ci.id.isEmpty()==false){
						    	//stop searching if we find any numbers
						    	break;
						    }
						}
	                     
	                    pCur.close();
	                }
	                
	                if(ci.photoUri!=null&&ci.id!=null&&!ci.id.isEmpty()){
	                	ContactInfo existing = indexed.get(ci.id);
	                	if(existing!=null){
	                		existing.photoUri = ci.photoUri;
	                		existing.name = ci.name;
	                		ci = existing;
	                	}

						
						ci.id = PhoneNumber.simplify(ci.id);
						
	                	publishProgress(ci);
	                }
	            }
	        }
	        
			return null;
		}
		
		
		
		@Override
		protected void onPostExecute(List<ContactInfo> result) {
			//because all data is published as it loads there is no need to update
			//Infact, result is null
			loaded = true;
			if(listener!=null)listener.onFinishedLoading(data);
			super.onPostExecute(result);
		}

		@Override
		protected void onProgressUpdate(ContactInfo... values) {
			LinkedList<ContactInfo> list = new LinkedList<ContactInfo>();
	
			for(ContactInfo ci: values){
				if(contactIds.containsKey(ci.id)==false){
					contactIds.put(ci.id, ci);
					data.add(ci);
					//TODO: insert at corret
					Collections.sort(data);
				}

				list.add(ci);
			}
			if(listener!=null)listener.onPartial(list, data);
			super.onProgressUpdate(values);
		}
		
		
		
	}
	
	public ContactInfo getSelf(){
		return self;
	}
	
	public String getOwnNumber(){
		if(self!=null){
			return self.id;
		}
		return null;
	}
	
	public void storeSelf(){
		SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();

		editor.putString(PHONE_NUMBER, self.id);
		editor.putString(PHONE_PRIVATE, self.secret);
		editor.putString(PHONE_PUBLIC, self.oneTimeSecret);
		
		editor.commit();
	}
	
	/**
	 * TODO: Actual storage...
	 * Saves to shared preferences
	 * @param ci
	 */
	public void updateContactInfo(ContactInfo ci){
		SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		
		Set<String> contacts = prefs.getStringSet(CONTACTS_KEY, new HashSet<String>());
		if(!contacts.contains(ci.id)){
			contacts.add(ci.id);
		}
		editor.putStringSet(CONTACTS_KEY, contacts);
		editor.putString(CONTACT_NAME+ci.id, ci.name);
		editor.putString(CONTACT_PHONE+ci.id, ci.id);
		editor.putString(CONTACT_SECRET+ci.id, ci.oneTimeSecret);
		
		editor.commit();
		
		
	}

	public ContactInfo getById(String id) {
		return contactIds.get(id);
	}

	public ContactInfo getByPhone(String phone) {
		for(ContactInfo ci: data){
			if(ci.id.equals(phone)){
				return ci;
			}
		}
		return null;
	}
	
	

	public void createContact(ContactInfo ci) {
		
		data.add(ci);
		Collections.sort(data);
		contactIds.put(ci.id,ci);
		updateContactInfo(ci);
		if(listener!=null)listener.onFinishedLoading(data);
		
	}

	public boolean isLoaded() {
		return loaded;
	}
}
