package si.evil.redphone.util;

import java.util.Random;

import si.evil.redphone.data.ContactInfo;
import android.nfc.NdefRecord;
import android.util.Base64;

public class NFCUtils {
	public static NdefRecord supportCreateMime(String mimeType, byte[] mimeData) {
        if (mimeType == null) throw new NullPointerException("mimeType is null");
        if (mimeType.length() == 0) throw new IllegalArgumentException("mimeType is empty");
        int slashIndex = mimeType.indexOf('/');
        if (slashIndex == 0) throw new IllegalArgumentException("mimeType must have major type");
        if (slashIndex == mimeType.length() - 1) {
            throw new IllegalArgumentException("mimeType must have minor type");
        }
        // missing '/' is allowed
        // MIME RFCs suggest ASCII encoding for content-type
        byte[] typeBytes = mimeType.getBytes();
        return new NdefRecord(NdefRecord.TNF_MIME_MEDIA, typeBytes, null, mimeData);
    }

	public static ContactInfo getContactFromData(String data) {
		// TODO Auto-generated method stub
		ContactInfo result =  new ContactInfo();
		if(data==null)return result;
		String[] tokens = data.split(":");
		if(tokens.length==3){
			result.id = tokens[0];
			result.id = tokens[0];
			result.oneTimeSecret = tokens[2];
			result.name = tokens[1];
		}
		
		return result;
	}
	
	public static String getPayloadFromContact(ContactInfo ci) {
		String result = ci.id+":"+ci.name+":"+ci.oneTimeSecret;
		return result;
	}

	public static String generateOneTimeSecretOld() {
		Random rng = new Random();
		byte [] buf = new byte[128];
		rng.nextBytes(buf);
		
		byte[] raw =  Base64.encode(buf, Base64.DEFAULT);
		return new String(raw);
	}
}
