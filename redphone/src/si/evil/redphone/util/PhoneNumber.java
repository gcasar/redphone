package si.evil.redphone.util;

public class PhoneNumber {
	/**
	 * Simplifies the phone number
	 * @param in
	 * @return
	 */
	public static String simplify(String in){
		String result = in.trim();
		result = result.replace(" ", "");
		result = result.replace("+","");
		
		//TODO: dejansko parsanje telefonske?
		
		if(result.startsWith("386")){
			result = result.substring(3);
			if(result.startsWith("0")==false)
				result = "0"+result;
		}
		
		
		return result;
	}
}
