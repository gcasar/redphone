package si.evil.redphone.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.google.android.gms.common.internal.k;

import android.util.Base64;

public class SecureUtils {
	KeyPairGenerator kpg;
    KeyPair kp;
    PublicKey publicKey;
    PrivateKey privateKey;
    byte [] encryptedBytes,decryptedBytes;
    Cipher cipher,cipher1;
    String encrypted,decrypted;
    
    public static KeyPair generateKey() throws NoSuchAlgorithmException{
    	KeyPair kp;
    	KeyPairGenerator kpg;
    	kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(1024);
        return kpg.genKeyPair();
    }
    
    public static String encodePrivate(PrivateKey k){
    	return Base64.encodeToString(k.getEncoded(),Base64.DEFAULT);
    }
    
    public static String encodePublic(PublicKey k){
    	return Base64.encodeToString(k.getEncoded(),Base64.DEFAULT);
    }

    public static String RSAEncrypt (String data, String publicRaw) throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException 
    {
       
    	byte[] pkRaw = Base64.decode(publicRaw, Base64.DEFAULT);
    	PublicKey pk = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(pkRaw));
		
        Cipher c = Cipher.getInstance("RSA");
        c.init(Cipher.ENCRYPT_MODE, pk);
        byte[] sendRaw = data.getBytes("UTF-8");
        byte[] resultRaw = c.doFinal(sendRaw);
        return Base64.encodeToString(resultRaw, Base64.DEFAULT);
    }

    public static String RSADecrypt (String data, String privateRaw) throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException 
    {
    	byte[] pkRaw = Base64.decode(privateRaw, Base64.DEFAULT);
    	PrivateKey pk = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(pkRaw));
		
        Cipher c = Cipher.getInstance("RSA");
        c.init(Cipher.DECRYPT_MODE, pk);
        byte[] resultRaw = c.doFinal(Base64.decode(data, Base64.DEFAULT));
        return new String(resultRaw, "UTF-8");

    }

}
